﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace image_deleter
{
    class Program
    {
        static void Main(string[] args)
        {
            WholeFileStructure fileStructure;


            {
                string fileContent;
                fileContent = File.ReadAllText(@"C:\Users\andriy.bilovus\Downloads\annotations_trainval2017\annotations\instances_train2017.json");
                fileStructure = JsonConvert.DeserializeObject<WholeFileStructure>(fileContent);
            }

            GC.Collect();

            var inputPath = @"";
            var outputPath = @"";
            foreach (var image in fileStructure.Images)
            {
                File.Copy(Path.Combine(inputPath, image.File_name).ToString(), Path.Combine(outputPath, image.File_name).ToString());
            }
        }
    }

    public class WholeFileStructure
    {
        public List<ImageData> Images { get; set; } = new List<ImageData>();

        public List<AnnotationData> Annotations { get; set; } = new List<AnnotationData>();

        public List<CategoryData> Categories { get; set; } = new List<CategoryData>();
    }

    public class ImageData
    {
        public string License { get; set; }

        public string File_name { get; set; }

        public string Coco_url { get; set; }

        public short Height { get; set; }

        public short Width { get; set; }

        public string Date_captured { get; set; }

        public string Flickr_url { get; set; }

        public long Id { get; set; }
    }

    public class AnnotationData
    {
        public long Id { get; set; }

        public long Category_id { get; set; }

        public long Image_id { get; set; }

        public short Iscrowd { get; set; }

        public string Area { get; set; }

        public List<string> Bbox { get; set; }

        public object Segmentation { get; set; }
    }


    public class CategoryData
    {
        public string Supercategory { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }
    }
}
